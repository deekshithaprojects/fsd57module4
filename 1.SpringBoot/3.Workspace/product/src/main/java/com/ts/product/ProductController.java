package com.ts.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {
	
        //Implementing Dependency Injection for ProductDao 
	@Autowired
	ProductDao productDao;
	
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {		
		return productDao.getAllProducts();
	}	
	@GetMapping("getProductById/{productId}")
	public Product getProductById(@PathVariable("productId") int productId) {
		return productDao.getProductById(productId);	
	}
	@GetMapping("getProductByName/{productName}")
	public List<Product> getProductByName(@PathVariable("productName") String productName) {
		return productDao.getProductByName(productName);
	}
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		return productDao.addProduct(product);
	}
	@PutMapping("updateProductById/{productId}")
    public Product updateProductById(@PathVariable("productId") int productId, @RequestBody Product updatedProduct) {
        return productDao.updateProductById(productId, updatedProduct);
    }
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int prodId) {
		productDao.deleteProductById(prodId);
		return "Product with ProductId: " + prodId + ", Deleted Successfully";
	}


}



